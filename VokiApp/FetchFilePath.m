//
//  FetchFilePath.m
//  VokiApp
//
//  Created by brst on 1/25/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "FetchFilePath.h"

@implementation FetchFilePath

- (id) init
{
    _jsNamespace = @"jsbridge";
    return self;
}

+ (FetchFilePath *)sharedMethod
{
    static FetchFilePath *sharedInstance;
    if (!sharedInstance)
    {
        sharedInstance = [[super alloc]init];
    }
    return sharedInstance;
}

-(NSArray*)FetchAssestData:(NSString*)path
{
    
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:path];
    NSError* err;
    NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&err];
    
    NSMutableArray *sortedArray = [[NSMutableArray alloc] initWithArray:[directoryContents sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    
    if([sortedArray count] != 0) {
        for (int i=0 ; i<sortedArray.count-1 ; i++ ) {
            
            NSString *str =sortedArray[i];
            if ([str integerValue] > 0)
            {
                [sortedArray addObject:str];
                [sortedArray removeObjectAtIndex:i];
                
                break;
            }
        }
    }
    
    
    //    sortedArray = [directoryContents sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
    //
    //        if ([obj1 integerValue] > [obj2 integerValue])
    //        {
    //            return (NSComparisonResult)NSOrderedDescending;
    //        }
    //        if ([obj1 integerValue] < [obj2 integerValue])
    //        {
    //            return (NSComparisonResult)NSOrderedAscending;
    //        }
    //        return (NSComparisonResult)NSOrderedSame;
    //    }];
    
    return sortedArray;
}

-(NSString*)GetBundlePath
{
    NSString* MainBundlePath = [[NSBundle mainBundle] resourcePath];
    return MainBundlePath;
}

@end
