//
//  FetchFilePath.h
//  VokiApp
//
//  Created by brst on 1/25/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface FetchFilePath : NSObject

@property (readonly) NSString* jsNamespace;

+ (FetchFilePath *)sharedMethod;
-(NSArray*)FetchAssestData:(NSString*)path;
-(NSString*)GetBundlePath;

@end
