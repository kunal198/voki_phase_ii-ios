//
//  ViewController.h
//  VokiApp
//
//  Created by brst on 12/8/15.
//  Copyright © 2015 brst. All rights reserved.
//z

#import <UIKit/UIKit.h>
#import "NKOColorPickerView.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <MessageUI/MessageUI.h>
#import <CoreMotion/CoreMotion.h>
#import "ILSaturationBrightnessPickerView.h"
#import "ILHuePickerView.h"
#import "lame.h"
#import <StoreKit/StoreKit.h>
#import <GooglePlus/GooglePlus.h>
#import "ScreenCaptureView.h"
#import "UCZProgressView.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "MKSHorizontalLineProgressView.h"
#import <TwitterKit/TwitterKit.h>


@class AppDelegate;

@interface ViewController : UIViewController<UIScrollViewDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate,FBSDKSharingDelegate, UIAlertViewDelegate,UIDocumentInteractionControllerDelegate,UIWebViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ILSaturationBrightnessPickerViewDelegate,ILHuePickerViewDelegate,UIGestureRecognizerDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver,SKRequestDelegate,SKStoreProductViewControllerDelegate,GPPSignInDelegate,GPPShareDelegate,UIDocumentInteractionControllerDelegate,UIWebViewDelegate,ScreenCaptureViewDelegate,UITextViewDelegate,UICollectionViewDelegateFlowLayout,TWTRComposerViewControllerDelegate,FBSDKSharingDialog>

{
    FBSDKLoginManager *login1;
    int playTime;
    bool fxaudioapicall;
    int  memorywarningcount;
    bool hasAddObserver;
    NSArray *validProducts;
    SKProduct *proUpgradeProduct;
    int sharecount;
    bool Dontaskagain;
    bool WithFxvalue;
    UIImageView *dollar;
    NKOColorPickerView *NKcolorPickerView;
    AppDelegate *appDelegate;
    CGRect myframe;
    float _RootViewHeight;
    float _stripWidth;
    int _previousTag;
    int currentIndexPath;
    int _previousAccessoryTag;
    int _previousVokiScrollerTag;
    int _previousImageTag;
    int _previousVokiTag;
    int _previousShareTag;
    NSString *output;
    int _textLanguageID;
    int _textVoiceID;
    int _textVoiceLangID;
    int angle1;
    int random;
    int engineID;
    bool isenable;
    NSIndexPath *item_idx;
    
    NSIndexPath *selectindexpath;
    
    
    BOOL videofbshare;
    BOOL Videotempsave;
    BOOL videotwittershare;
    NSString *Main_videoPath;
    BOOL sameaudio;
     NSURL *outurltweet;
    BOOL Acesstotweet;
    BOOL TweeterAcess;
    
    UIImage *fbshareimage;
    UIButton *Appbtn;
    UIButton *button1;
    NSString *selecteimagpath;
    
    IBOutlet UIButton *purchasebtn;
    NSMutableArray *Allcatagory;
    NSMutableArray *Allcaracters;
    NSArray *freecharacetr;
    NSMutableDictionary *Allcatagorys;
    bool oritentationLeft;
    
    IBOutlet UIButton *Groupup;
    IBOutlet UIButton *Groupdown;
    IBOutlet UIView *allcatagoryview;
    UIButton *selectbtn;
    
    IBOutlet UICollectionView *allcatacollectionview;
    
    NSMutableArray *shareScrollerArray;
    
    IBOutlet UITextView *GotoFootertext;
    float _progressIncrement;
    bool israndomcomplete;
    BOOL _isRecording;
    BOOL _isAccessoriesSelected;
    BOOL _isRightVokiOpened;
    BOOL _isMainButtonCalled;
    
    BOOL _isRightShareOpen;
    BOOL _isRightMenuOpen;
    BOOL isitunesconnect;
    BOOL isgroupopen;
    BOOL SharingAvailble;
    
    BOOL Ischrrecording;
    BOOL inAppPurchaseCount_bool;
    
    int constant;
    int m;
    NSArray *_photos;
    NSString *  partofBody;
    NSTimer *_progressTimer;
    AVAudioRecorder* recorder;
    AVAudioPlayer* audioPlayer;
    NSString* recorderFilePath;
    SystemSoundID soundID;
    NSMutableArray *_textLanguageArray;
    NSMutableArray *_textMainVoiceIDArray;
    NSMutableArray *_textVoiceArray;
    NSMutableArray *restore_products;
    NSMutableArray *_XMLDataArray;
    NSMutableArray *CoreDataArray;
    NSMutableArray *ImageScrollerArray;
    NSString *MainBundlePath;
    NSMutableArray *VokiScrollerArray, *VokiContentArray;
    NSMutableArray *accessoryListArray;
    NSMutableArray *AccessoryDataarray;
    NSMutableArray *purchasedItemIDs;
    NSDictionary *Allproductid;
    NSString *Productforbuy;
    float _canvasWidth;
    float _canvasHeight;
    NSTimer *MemoryCleaner;
    BOOL play;
    bool stop;
    bool recording;
    bool hideallview;
    bool rightmenu;
    bool buyall;
    BOOL isbuyall;
    bool alertshown;
    BOOL  Vokilogoopen;
    bool purchaseopen;
    bool recordprocess;
    IBOutlet UIImageView *purchasedollar;
    IBOutlet UILabel *PremiumLabel;
    IBOutlet UIImageView *PurchaseWhyALlimg;
    NSMutableArray *imageArray;
    
    NSTimer *myTimerName;
    
    
    IBOutlet UIImageView *vokilogo_img;
    
    IBOutlet UIImageView *vokilogo_imgagess;
    
    
    IBOutlet UIPageControl *Pagecontrol;
    
    IBOutlet UIImageView *imageViewForDownloadingImages;
    IBOutlet UIImageView *imageView2ForDownloadingImages;
    IBOutlet UIImageView *imageView3ForDownloadingImages;

    
    
    IBOutlet UICollectionView *sliding_collectionview;
    
    IBOutlet UIImageView *download_installimg;
    IBOutlet UIImageView *imageAfterSplash;

    IBOutlet UIView *Sliding_view;
    JSContext *javascriptContext;
    
    BOOL isrecording;
    BOOL isfirstecord;
    
    //For Google and Mail sharing
    
    BOOL Googleshared;
    BOOL Mailshared;
    BOOL whatsappshared;
    BOOL ishalfrecording;
    
    
    

}

@property(nonatomic, copy) AVVideoComposition *videoComposition;

@property (nonatomic, retain) NSURLConnection * downloadingConnection;


@property (weak, nonatomic) IBOutlet MKSHorizontalLineProgressView *progressview;


@property (strong, nonatomic) IBOutlet UIView *downloadview;


@property (nonatomic) NSMutableData *data;
@property (nonatomic) double expectedBytes;
@property (retain) UIDocumentInteractionController * documentInteractionController;
@property (strong, nonatomic) SKProductsRequest *productsRequest;
@property (strong, nonatomic) SKProduct *product;

@property (strong, nonatomic) IBOutlet UIImageView *whyloginclosepng;
@property (strong, nonatomic) SKReceiptRefreshRequest *ReceiptRequest;
@property (strong, nonatomic) IBOutlet ScreenCaptureView *screencapture;
@property (strong, nonatomic) IBOutlet ScreenCaptureView *captureview;
@property (strong, nonatomic) IBOutlet UIButton *deleteGalleryBtn;
@property (strong, nonatomic)  AVAssetReader *assetReader;
@property (strong) AVAssetWriter *videoWriter;
@property (strong, nonatomic)  AVAssetWriterInputPixelBufferAdaptor  *adaptor;

@property (strong, nonatomic) IBOutlet UIButton *playCharacterAud;

@property (retain, nonatomic) UIWebView *__CharacterWebView;
@property (strong, nonatomic) UIWebView *__CharacterWebview2;

@property (strong, nonatomic) IBOutlet UIImageView *testingImage;
@property (nonatomic, retain) UIDocumentInteractionController *documentController;
@property (strong, nonatomic) IBOutlet UIButton *MainBGButton;

- (IBAction)MainBgAction:(id)sender;
- (IBAction)PlayCharacterAudio:(id)sender;
- (IBAction)PlayCharAudiodup:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *Purchasebuyallbtn;

- (IBAction)Purchaserestoreall:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *whybuyavatartstext;


@property (strong, nonatomic) IBOutlet UIButton *PlayCharAud1dup;

@property (weak, nonatomic) IBOutlet UIImageView *menuAboutLine;
@property (weak, nonatomic) IBOutlet UIImageView *menutermLine;
@property (weak, nonatomic) IBOutlet UIImageView *menuFaqLine;

@property (strong, nonatomic) IBOutlet UIView *_mainFirstStrip;
@property (weak, nonatomic) IBOutlet UIButton *ttsMiceBtn;

@property (weak, nonatomic) IBOutlet UIButton *recordTypeBtn;

@property (strong, nonatomic) IBOutlet UIButton *_vokiButton;
@property (strong, nonatomic) IBOutlet UIButton *_accessoriesButton;
@property (strong, nonatomic) IBOutlet UIButton *_imageButton;
@property (strong, nonatomic) IBOutlet UIButton *_colorButton;
@property (strong, nonatomic) IBOutlet UIButton *_voiceButton;
@property (strong, nonatomic) IBOutlet UIButton *_shareButton;
@property (strong, nonatomic) IBOutlet UIButton *_firstStripCloseButton;

- (IBAction)VokiAction:(id)sender;
- (IBAction)AccessoriesAction:(id)sender;
- (IBAction)ImageAction:(id)sender;
- (IBAction)ColorAction:(id)sender;
- (IBAction)VoiceAction:(id)sender;
- (IBAction)ShareAction:(id)sender;
- (IBAction)FirstStripCloseAction:(id)sender;
- (IBAction)LearnMore:(id)sender;
- (IBAction)deleteGalleryImageBtn:(id)sender;


@property (strong, nonatomic) IBOutlet ILHuePickerView *huePickerColor;
@property (strong, nonatomic) IBOutlet ILSaturationBrightnessPickerView *brightNessPicker;

@property (weak, nonatomic) IBOutlet UIView *fxMenuView;
@property (weak, nonatomic) IBOutlet UITableView *fxTableView;
@property (strong, nonatomic) IBOutlet UITextField *twxtUserName;

@property (strong, nonatomic) IBOutlet UIView *whyloginbackview;
//@property (strong, nonatomic) IBOutlet UIImageView *imgClose;

@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) UILabel *itemName;
@property (strong, nonatomic) IBOutlet UIView *_clothingView;
@property (strong, nonatomic) IBOutlet UIScrollView *MouthViewScroller;

@property (strong, nonatomic) IBOutlet UIButton *_clothButton;
@property (strong, nonatomic) IBOutlet UIButton *_capButton;
@property (strong, nonatomic) IBOutlet UIButton *_glassButton;
@property (strong, nonatomic) IBOutlet UIButton *_blingButton;
@property (strong, nonatomic) IBOutlet UIButton *_propButton;
@property (strong, nonatomic) IBOutlet UIButton *_hair_Button;
@property (strong, nonatomic) IBOutlet UIButton *_fHairbutton;
@property (strong, nonatomic) IBOutlet UIButton *_mouth_Button;
@property (strong, nonatomic) IBOutlet UIButton *_MouthViewUpBtn;
@property (strong, nonatomic) IBOutlet UIButton *_MouthViewdownBtn;



- (IBAction)ClothAction:(id)sender;
- (IBAction)CapAction:(id)sender;
- (IBAction)GlassAction:(id)sender;
- (IBAction)BlingAction:(id)sender;
- (IBAction)PropsAction:(id)sender;
- (IBAction)Hair_Action:(id)sender;
- (IBAction)fHairAction:(id)sender;
- (IBAction)Mouth_Action:(id)sender;
- (IBAction)MouthViewUpAction:(id)sender;
- (IBAction)MouthViewDownAction:(id)sender;




@property (strong, nonatomic) IBOutlet UIView *_vokiView;

@property (strong, nonatomic) IBOutlet UIButton *_vokiUpArrow;
@property (strong, nonatomic) IBOutlet UIButton *_vokiDownArrow;
@property (strong, nonatomic) IBOutlet UIButton *_vokiDiceBtn;

- (IBAction)VokiUpAction:(id)sender;
- (IBAction)VokiDownAction:(id)sender;
- (IBAction)VokiDiceAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *_vokiScroller;



@property (strong, nonatomic) IBOutlet UIView *_thirdView;

@property (strong, nonatomic) IBOutlet UIButton *_thirdUpArrow;
@property (strong, nonatomic) IBOutlet UIButton *_thirdDownArrow;

- (IBAction)ThirdUpAction:(id)sender;
- (IBAction)ThirdDownAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *_thirdContentScroller;
@property (strong, nonatomic) IBOutlet UIButton *CloeseThirdStripBtn;
@property (strong, nonatomic) IBOutlet UIButton *CloseSecondStripBtn;
- (IBAction)CloseThirdStripAction:(id)sender;
- (IBAction)CloseSecondStripAction:(id)sender;



@property (strong, nonatomic) IBOutlet UIView *_ImageView;

@property (strong, nonatomic) IBOutlet UIButton *_ImageUpArrow;
@property (strong, nonatomic) IBOutlet UIButton *_ImageDownArrow;

- (IBAction)ImageUpAction:(id)sender;
- (IBAction)ImageDownAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *_ImageContentScroller;


@property (strong, nonatomic) IBOutlet UIView *_colorPickerView;
@property (strong, nonatomic) IBOutlet UIButton *_closePickerView;
@property (strong, nonatomic) IBOutlet UIButton *_mouthButton;
@property (strong, nonatomic) IBOutlet UIButton *_eyeButton;
@property (strong, nonatomic) IBOutlet UIButton *_handButton;
@property (strong, nonatomic) IBOutlet UIButton *_hairButton;

- (IBAction)ClosePickerAction:(id)sender;
- (IBAction)HandAction:(id)sender;
- (IBAction)MouthAction:(id)sender;
- (IBAction)EyeAction:(id)sender;
- (IBAction)HairAction:(id)sender;


//----------record voice view-----------//

@property (strong, nonatomic) IBOutlet UIView *_recordVoiceView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtnTable;
@property (strong, nonatomic) IBOutlet UIImageView *_ProgressBGImage;
@property (strong, nonatomic) IBOutlet UIButton *_typeButton;
@property (strong, nonatomic) IBOutlet UIButton *_miceButton;
@property (strong, nonatomic) IBOutlet UIImageView *_progressImage;
@property (strong, nonatomic) IBOutlet UIButton *_recordButton;
@property (strong, nonatomic) IBOutlet UIButton *_dropDownBtn;
@property (strong, nonatomic) IBOutlet UIImageView *_voiceDropDownImg;
@property (strong, nonatomic) IBOutlet UILabel *_voiceTypeLBL;
@property (strong, nonatomic) IBOutlet UILabel *_recordYourVoiceLbl;
@property (strong, nonatomic) IBOutlet UILabel *_uptoTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel *_zeroLbl;
@property (strong, nonatomic) IBOutlet UILabel *_halfTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel *_timeOverLbl;

- (IBAction)TypeAction:(id)sender;
- (IBAction)MiceAction:(id)sender;
- (IBAction)RecordAction:(id)sender;
- (IBAction)DropDownAction:(id)sender;

- (IBAction)LoginAction:(id)sender;

//--------- Voice text view ---------//

@property (strong, nonatomic) IBOutlet UIView *_voiceTextView;

- (IBAction)cancelTableBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *VokiDiceImage;
@property (weak, nonatomic) IBOutlet UIView *BgViewForTable;

@property (strong, nonatomic) IBOutlet UIImageView *_text_textViewBgImg;
@property (strong, nonatomic) IBOutlet UIImageView *_text_languageBGImg;
@property (strong, nonatomic) IBOutlet UIImageView *_text_nameBGImg;
@property (strong, nonatomic) IBOutlet UIImageView *_text_fileTypeBGImg;
@property (strong, nonatomic) IBOutlet UIView *_text_Languageview;
@property (strong, nonatomic) IBOutlet UITableView *_textLanguageTable;
@property (strong, nonatomic) IBOutlet UITableView *_text_voiceIDTable;
@property (strong, nonatomic) IBOutlet UIView *_text_VoiceIDView;
@property (strong, nonatomic) IBOutlet UIButton *_textVoiceSubmitBtn;
@property (strong, nonatomic) IBOutlet UIView *_loadeMainView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *_ActivityIndicator;
@property (strong, nonatomic) IBOutlet UIView *_ActivityView;



@property (strong, nonatomic) IBOutlet UIButton *purchasebuyall;

@property (strong, nonatomic) IBOutlet UILabel *_LanguageLbl;
@property (strong, nonatomic) IBOutlet UILabel *_nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *_fileTypeLbl;
@property (strong, nonatomic) IBOutlet UITextView *_descriptionTextView;
@property (strong, nonatomic) IBOutlet UIButton *_languageDropDownBtn;
@property (strong, nonatomic) IBOutlet UIButton *_nameDropDownBtn;
@property (strong, nonatomic) IBOutlet UIButton *_fileTypeDropDownBtn;

- (IBAction)Text_LanguageAction:(id)sender;
- (IBAction)Text_nameAction:(id)sender;
- (IBAction)Text_fileTypeAction:(id)sender;
- (IBAction)submitTextAction:(id)sender;



@property (strong, nonatomic) IBOutlet UIButton *_shareUpArrowBtn;
@property (strong, nonatomic) IBOutlet UIButton *_saveToLibBtn;
@property (strong, nonatomic) IBOutlet UIButton *_shareDownArrowBtn;
@property (strong, nonatomic) IBOutlet UIView *_shareView;
@property (strong, nonatomic) IBOutlet UIScrollView *_shareScroller;

- (IBAction)SaherUpAction:(id)sender;
- (IBAction)ShareDownAction:(id)sender;
- (IBAction)SaveToLibAction:(id)sender;


#pragma mark Right SharingView

@property (strong, nonatomic) IBOutlet UIButton *_rightVokiBtn;
@property (strong, nonatomic) IBOutlet UIView *_rightShareView;
@property (strong, nonatomic) IBOutlet UIButton *_rightShareBtn;
@property (strong, nonatomic) IBOutlet UIButton *_facebookBtn;
@property (strong, nonatomic) IBOutlet UIButton *_twitterBtn;
@property (strong, nonatomic) IBOutlet UIButton *_googleBtn;
@property (strong, nonatomic) IBOutlet UIButton *_wBtn;
@property (strong, nonatomic) IBOutlet UIButton *_blogBtn;
@property (strong, nonatomic) IBOutlet UIButton *_mailBtn;
@property (strong, nonatomic) IBOutlet UIButton *_closeRightViewBtn;
@property (strong, nonatomic) IBOutlet UIButton *Whatsbtn;


- (IBAction)RightShareAction:(id)sender;
- (IBAction)FacebookAction:(id)sender;
- (IBAction)TwitterAction:(id)sender;
- (IBAction)GoogleAction:(id)sender;
- (IBAction)MailAction:(id)sender;
- (IBAction)whatsppAction:(id)sender;


//-------- right menu methods ---------//

@property (strong, nonatomic) IBOutlet UIView *RightVokiLogoView;
@property (strong, nonatomic) IBOutlet UIButton *_closerightMenuNewBtn;
@property (strong, nonatomic) IBOutlet UIButton *getImageBtn;
@property (strong, nonatomic) IBOutlet UIButton *video_downloadbtn;
@property (strong, nonatomic) IBOutlet UIView *_rightMenuView;
@property (strong, nonatomic) IBOutlet UIButton *_aboutBtn;
@property (strong, nonatomic) IBOutlet UIButton *_syncBtn;
@property (strong, nonatomic) IBOutlet UIButton *_termsBtn;
@property (strong, nonatomic) IBOutlet UIButton *_faqBtn;
@property (strong, nonatomic) IBOutlet UITextView *_termsServicesTextView;
@property (strong, nonatomic) IBOutlet UITextView *_aboutTextView;
@property (strong, nonatomic) IBOutlet UITextView *_syncTextView;
@property (strong, nonatomic) IBOutlet UITextView *_faqTextView;
@property (strong, nonatomic) IBOutlet UIButton *leftmovemenubtn;
@property (strong, nonatomic) IBOutlet UIButton *closemainStripBtn;

@property (strong, nonatomic) IBOutlet UIButton *colorMakeUp;
@property (strong, nonatomic) IBOutlet UIButton *colorblush;
- (IBAction)makeUp:(id)sender;
- (IBAction)blush:(id)sender;
- (IBAction)AboutAction:(id)sender;
- (IBAction)SyncAction:(id)sender;
- (IBAction)TermsAction:(id)sender;
- (IBAction)FaqAction:(id)sender;
- (IBAction)leftMenumoveBtn:(id)sender;
- (IBAction)closeMainStripBtn:(id)sender;
- (IBAction)video_downloadAction:(id)sender;
- (IBAction)getImageBtn:(id)sender;
- (IBAction)RightVokiAction:(id)sender;
//------- close right view btn -------//

- (IBAction)CloseRightViews:(id)sender;

@property (nonatomic, strong) CMMotionManager *motionManager;
@property (strong, nonatomic) IBOutlet UIView *_menuDescriptionView;
@property (strong, nonatomic) IBOutlet UIView *_rightMainMenuView;
@property (strong, nonatomic) IBOutlet UIButton *_aboutMainBtn;
@property (strong, nonatomic) IBOutlet UIButton *_syncMainBtn;
@property (strong, nonatomic) IBOutlet UIButton *_termsMainBtn;
@property (strong, nonatomic) IBOutlet UIButton *_faqMainBtn;
@property (strong, nonatomic) IBOutlet UIView *_aboutView;
@property (strong, nonatomic) IBOutlet UIView *_syncView;
@property (strong, nonatomic) IBOutlet UIView *_termsView;
@property (strong, nonatomic) IBOutlet UIView *_faqView;
@property (strong, nonatomic) IBOutlet UIView *Accountview;
@property (strong, nonatomic) IBOutlet UIView *Whyloginview;
@property (strong, nonatomic) IBOutlet UIView *Purchaseview;

@property (strong, nonatomic) IBOutlet UILabel *LoginErroMsg;
@property (strong, nonatomic) IBOutlet UILabel *AccountLabel;
@property (strong, nonatomic) IBOutlet UILabel *whyloginlabel;
@property (strong, nonatomic) IBOutlet UILabel *LoginuserName;

@property (strong, nonatomic) IBOutlet UIView *unzipview;

@property (strong, nonatomic) IBOutlet UILabel *purchaseheaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *restoretext;
@property (strong, nonatomic) IBOutlet UILabel *purchaseheaderlabel;

@property (strong, nonatomic) IBOutlet UIButton *restorebtn;
@property (strong, nonatomic) IBOutlet UIButton *forgotpassword;
@property (strong, nonatomic) IBOutlet UIButton *loginbtn;
@property (strong, nonatomic) IBOutlet UIButton *closewhyloginbtn;
@property (strong, nonatomic) IBOutlet UIButton *acclearnmore;
@property (strong, nonatomic) IBOutlet UIButton *Purchaselearnmorebtn;

@property (strong, nonatomic) IBOutlet UIView *purchasedallview;
@property (strong, nonatomic) IBOutlet UIView *purchaselearnmore;
@property (strong, nonatomic) IBOutlet UIView *purchaseloginbackview;
@property (strong, nonatomic) IBOutlet UIView *MainLoginview;
@property (strong, nonatomic) IBOutlet UIView *MultipleLogins;
@property (strong, nonatomic) IBOutlet UIView *multipleloginborderview;
@property (strong, nonatomic) IBOutlet UIView *Loginusrdetailview;


@property (strong, nonatomic) IBOutlet UITextView *purchaselogintextview;
@property (strong, nonatomic) IBOutlet UITextView *multiplelogforgetLink;
@property (strong, nonatomic) IBOutlet UITextView *whylogintextview;

@property (strong, nonatomic) IBOutlet UIImageView *Purchasedollerimg;
@property (strong, nonatomic) IBOutlet UIImageView *rederrorimg;

@property (strong, nonatomic) IBOutlet UIScrollView *Purchasescroolview;
@property (strong, nonatomic) IBOutlet UILabel *LoginheaderLabel;


//--------------- // ----------------- //
#pragma mark - Play and Stop button
@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UIButton *stopBtn;

- (IBAction)startPlaying:(id)sender;
- (IBAction)stopPlaying:(id)sender;


#pragma mark - LongPressGestures
- (IBAction)vokiBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)accessoriesBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)imageBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)colorBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)voiceBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)shareBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)addBackgroundBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)saveTiLibBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)clothBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)hairBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)capBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)glassBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)mouthBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)blingBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)facialBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)propBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)colorMouthBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)colorEyeBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)colorHandBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)colorHairBtnGesture:(UILongPressGestureRecognizer *)sender;
- (IBAction)panGestureToMoveChart:(UIPanGestureRecognizer *)sender;

@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *pangestureMovetoChar;
//------------//---------------//

#pragma mark - Purchase Alert
@property (strong, nonatomic) IBOutlet UIButton *sidedollar;
- (IBAction)sidedollar:(id)sender;


#pragma mark - Right view Actions

- (IBAction)RestoreAction:(id)sender;
- (IBAction)purchaselearnAction:(id)sender;
- (IBAction)signout:(id)sender;
- (IBAction)Logoutotherdevice:(id)sender;
- (IBAction)ForgotPassword:(id)sender;
- (IBAction)Purchaseaction:(id)sender;
- (IBAction)closepurchaselearnmore:(id)sender;
- (IBAction)purshaseBuyallaction:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *TermsandPrivacyteextview;

@property (strong, nonatomic) IBOutlet UITextView *Privacytextview;

@property (strong, nonatomic) IBOutlet UIView *Termsprivacyview;

@property (strong, nonatomic) IBOutlet UIView *Privacyview;

- (IBAction)closeprivacyview:(id)sender;

@property (nonatomic) UIBlurEffect *blurEffect NS_AVAILABLE_IOS(8_0);

@property (nonatomic, readonly) UILabel *textLabel1;
- (IBAction)closeTermsprivacyview:(id)sender;

@property (nonatomic) CALayer *backgroundLayer;
@property (strong, nonatomic) IBOutlet UILabel *purchasedetailLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *purchasemainscroolview;
@property (nonatomic, strong)  UIActivityIndicatorView *transparentIndicator;


@property (nonatomic, strong) UIImageView *splashView;

////////

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *screenCaptureTopCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *screenCaptureBottomCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *screenCaptureTrailingCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *screenCaptureLeadingCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *captureViewBottomCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *captureViewTopCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *captureViewLeadingCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *captureViewTrailingCons;


///////////////////----------------///////////////////////////////

-(NSArray*)getRGBAFromImage:(UIImage*)image atx:(int)xp atY:(int)yp;
-(NSString *)hexValuesFromUIColor:(UIColor *)color;

+(void)twittervideoresult;

@end

