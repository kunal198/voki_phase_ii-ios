//
//  GalleryViewController.h
//  VokiApp
//
//  Created by mrinal khullar on 3/8/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface GalleryViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIPopoverController *popover;
    BOOL orientation;
    UIImagePickerController *imagePicker ;
}

@property (nonatomic) BOOL isPresented;
@property (nonatomic) BOOL galleryCamera;
@property (nonatomic,strong) UIImage *galleryImage;

@end
