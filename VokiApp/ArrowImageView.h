//
//  ArrowImageView.h
//  VokiApp
//
//  Created by Brst981 on 25/04/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ArrowImageDelegate <NSObject>

-(void) touchChanges:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event ;
@end


@interface ArrowImageView : UIView
{
    UIImageView * imageView;
}
@property(nonatomic,weak) id<ArrowImageDelegate>delegate;
@property(nonatomic,strong,setter=setImage:)UIImage *image;
@end
