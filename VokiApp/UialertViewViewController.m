//
//  UialertViewViewController.m
//  Pure
//
//  Created by netset on 10/6/15.
//  Copyright (c) 2015 Rahul Mehndiratta. All rights reserved.
//

#import "UialertViewViewController.h"

@interface UialertViewViewController ()
{
    NSRange range;
}
@end

@implementation UialertViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
//    NSString *Restoretext = @"<font color=\"white\"><u>restore purchases</u></font>";
//    
//    NSAttributedString *Attributted = [[NSAttributedString alloc] initWithData:[Restoretext dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    self.Alertrestorelabel.attributedText = Attributted;
//    self.Alertrestorelabel.tintColor = [UIColor orangeColor];
//    
//    
//    NSString *Learnmoretext = @"<font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\"><u> Learn More</u></span></font>";
//    
//    NSAttributedString *Attributtedlearnmore = [[NSAttributedString alloc] initWithData:[Learnmoretext dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    [self.unblocklearnmore setAttributedTitle:Attributtedlearnmore forState:UIControlStateNormal];
//
//    self.unblocklearnmore.tintColor = [UIColor orangeColor];
//
//    NSString *vokilogintext = @"<font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\"><u>(Login) </u></span></font>";
//    NSAttributedString *Attributtedlogin= [[NSAttributedString alloc] initWithData:[vokilogintext dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    [self.vokilogin setAttributedTitle:Attributtedlogin forState:UIControlStateNormal];
//    self.vokilogin.tintColor = [UIColor blueColor];
//    
//    
//    _buyallavatarstext.textAlignment = NSTextAlignmentJustified;
//
//    NSString *htmlStrin = @"<font color=\"white\"><span style=\"font-family: Arial ; font-size: 18\">Or unlock hundreds of <br> voki avatars.</font></span><font color=\"#fc7d00\"><span style=\"font-family: Arial; font-size: 18\"><u>Learn More</u></span>.</font>";
//    
//    NSAttributedString *attributedStrin = [[NSAttributedString alloc] initWithData:[htmlStrin dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    _unblockcharORLabel.attributedText = attributedStrin;
//    
//    NSString *htmlString = @"<font color=\"white\"><span style=\"font-family: Arial; font-size: 20\"><u>restore purchases</u></span>.</font>";
//    
//    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    _Alertrestorelabel.attributedText = attributedString;
    
    _blackView.layer.cornerRadius=15;
    _blackView.clipsToBounds=YES;
    
    _whiteborderView.layer.cornerRadius=15;
    _whiteborderView.clipsToBounds=YES;
    _whiteborderView.layer.borderWidth = 2;
    _whiteborderView.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    
    
    _ratebackview.layer.cornerRadius=15;
    _ratebackview.clipsToBounds=YES;
    _ratebackview.layer.borderWidth = 2;
    _ratebackview.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    
    _deleteView.layer.cornerRadius=15;
    _deleteView.clipsToBounds=YES;
    _deleteView.layer.borderWidth = 2;
    _deleteView.layer.borderColor = [[UIColor whiteColor]CGColor];
    
//    _ratealert.layer.cornerRadius=15;
//    _ratealert.clipsToBounds=YES;
//    _ratealert.layer.borderWidth = 2;
//    _ratealert.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    _unblockLearnmore.layer.cornerRadius=15;
    _unblockLearnmore.clipsToBounds=YES;
    _unblockLearnmore.layer.borderWidth = 2;
    _unblockLearnmore.layer.borderColor = [[UIColor whiteColor]CGColor];
    _unblockvookichar.layer.borderColor = [[UIColor whiteColor]CGColor];

    _thankyoualert.layer.cornerRadius=15;
    _thankyoualert.clipsToBounds=YES;
    _thankyoualert.layer.borderWidth = 2;
    _thankyoualert.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    
    _nobtn.layer.cornerRadius=15;
    _nobtn.clipsToBounds=YES;
    _nobtn.layer.borderWidth = 2;
    
    _yesBtn.layer.cornerRadius=15;
    _yesBtn.clipsToBounds=YES;
    _yesBtn.layer.borderWidth = 2;
  
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        _whiteborderView.layer.borderWidth = 5;
        _deleteView.layer.borderWidth = 5;
        _lblText.font = [UIFont systemFontOfSize:20];
        _deleteCharLbl.font = [UIFont systemFontOfSize:20];
    }

    CGFloat borderWidth = 3.0f;
    _unblockvookichar.layer.borderColor = [UIColor whiteColor].CGColor;
    _unblockvookichar.layer.borderWidth = borderWidth;
    
    _unblockLearnmore.layer.borderColor = [UIColor whiteColor].CGColor;
     _unblockLearnmore.layer.borderWidth = borderWidth;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        
        }
    
        
    
    
    
    
    
    
    
    // Dispose of any resources that can be recreated.
}



- (IBAction)closeBtn:(id)sender
{
    
}

-(UIView *)showPopUp
{
    return self.view;
}

- (IBAction)LearnMore:(id)sender {
}


@end
