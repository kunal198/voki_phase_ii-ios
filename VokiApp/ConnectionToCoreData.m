//
//  SaveToCoreData.m
//  VokiApp
//
//  Created by brst on 2/1/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "ConnectionToCoreData.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@implementation ConnectionToCoreData

+ (ConnectionToCoreData *)sharedMethod
{
    static ConnectionToCoreData *sharedInstance;
    if (!sharedInstance)
    {
        sharedInstance = [[super alloc]init];
    }
    return sharedInstance;
}

-(void)SaveIntoCoreData:(NSMutableArray*)DataArray
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSArray *AccArray = [[NSArray alloc]initWithObjects:@"Costume", @"fhair", @"Glasses", @"Hair", @"Hat", @"mouth", @"Necklace", @"Props", nil];
    
    for(int i = 0; i < DataArray.count; i++)
    {
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSManagedObject *transaction = [NSEntityDescription insertNewObjectForEntityForName:@"TblCharacter" inManagedObjectContext:context];
        
        [transaction setValue:[[DataArray objectAtIndex:i] valueForKey:@"accString"] forKey:@"accString"];
        [transaction setValue:[[DataArray objectAtIndex:i] valueForKey:@"colorString"] forKey:@"colorString"];
        [transaction setValue:[[DataArray objectAtIndex:i] valueForKey:@"spriteSheet"] forKey:@"spriteSheet"];
        
        NSArray *characterArray = [[[DataArray objectAtIndex:i] valueForKey:@"spriteSheet"] componentsSeparatedByString:@"/"];
        
        [transaction setValue:[characterArray objectAtIndex:characterArray.count-2] forKey:@"character_name"];
        
        [self SaveAccessoriesToDB:@"Type" :transaction :AccArray :DataArray :i];
        [self SaveAccessoriesToDB:@"Types" :transaction :AccArray :DataArray :i];
        
        // Save the context
        NSError *error = nil;
        if (![context save:&error])
        {
            NSLog(@"Save Failed! %@ %@", error, [error localizedDescription]);
        }
        else
        {
            NSLog(@"Data saved properly");
        }
    }
}

-(void)SaveAccessoriesToDB:(NSString*)FirstKey :(NSManagedObject*)transaction :(NSArray*)AccArray :(NSMutableArray*)DataArray :(int)index
{
    for(int x = 0; x < [AccArray count]; x++)
    {
        int check = 0;
        for(int y = 0; y < [[[DataArray objectAtIndex:index] valueForKey:FirstKey] count]; y++)
        {
            for (NSString *key in [[[DataArray objectAtIndex:index] valueForKey:FirstKey] objectAtIndex:y])
            {
                if([key isEqualToString:[AccArray objectAtIndex:x]])
                {
                    if([FirstKey isEqualToString:@"Types"])
                    {
                        [transaction setValue:[[[[DataArray objectAtIndex:index] valueForKey:FirstKey]objectAtIndex:y]valueForKey:key] forKey:[NSString stringWithFormat:@"%@_list",[key lowercaseString]]];
                    }
                    else
                    {
                        [transaction setValue:[[[[DataArray objectAtIndex:index] valueForKey:FirstKey]objectAtIndex:y]valueForKey:key] forKey:[NSString stringWithFormat:@"default_%@",[key lowercaseString]]];
                    }
                    check = 1;
                }
            }
        }
        
        if(check == 0)
        {
            if([FirstKey isEqualToString:@"Types"])
            {
                [transaction setValue:@"0" forKey:[NSString stringWithFormat:@"%@_list",[[AccArray objectAtIndex:x] lowercaseString]]];
            }
            else
            {
                [transaction setValue:@"0" forKey:[NSString stringWithFormat:@"default_%@",[[AccArray objectAtIndex:x] lowercaseString]]];
            }
        }
    }
}

-(NSMutableArray*)FetchDataFromLocalDB
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TblCharacter" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray* Array = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *MutableArray = [NSMutableArray new];
    [MutableArray addObjectsFromArray:Array];
    
    return MutableArray;
}

-(NSMutableArray*)FetchDataWithWhereClause:(NSString*)keyString :(NSString*)ObjString
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TblCharacter" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"character_name == %@",ObjString];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *MutableArray = [NSMutableArray new];
    [MutableArray addObjectsFromArray:fetchedObjects];
    
    return MutableArray;
}

@end
