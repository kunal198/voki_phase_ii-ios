//
//  viewCollectionViewCell.h
//  VokiApp
//
//  Created by Brst981 on 18/07/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface viewCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *characterimg;

@property (strong, nonatomic) IBOutlet UIImageView *dollarimg;


@property (strong, nonatomic) IBOutlet UIImageView *splashimg;

@end
