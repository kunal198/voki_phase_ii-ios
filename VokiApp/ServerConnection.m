//
//  ServerConnection.m
//  VokiApp
//
//  Created by brst on 1/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import "ServerConnection.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Md5Converter.h"
#import "StringEncoder.h"
#include <dispatch/dispatch.h>

@implementation ServerConnection

+ (ServerConnection *)sharedMethod
{
    static ServerConnection *sharedInstance;
    if (!sharedInstance)
    {
        sharedInstance = [[super alloc]init];
    }
    return sharedInstance;
}

-(NSString*)SendDataToServer:(NSString*)EID :(NSString*)LID :(NSString*)VID :(NSString*)TXT :(NSString*)EXT :(NSString*)FX_TYPE :(NSString*)FX_LEVEL :(NSString*)ACC :(NSString*)SESSION :(NSString*)SecretKey


{
    //http://cache.oddcast.com/tts/gen.php?EID=2&LID=19&VID=2&TXT=hello&EXT=mp3&FX_TYPE=&FX_LEVEL=&ACC=
   // 5603124&SESSION=&CS=3ff28e266dcaa2db7b20e484e5d8801c
   // [self.view removefr:audioActivity];
    

    appDelegate_obj = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://cache.oddcast.com/tts/gen.php?"]];
    [request setHTTPMethod:@"POST"];
    
//    EID = @"6";
//    LID = @"1";
//    VID = @"3";
//    TXT = @"testing the app";
//    EXT = @"mp3";
//    FX_TYPE = @"";
//    FX_LEVEL = @"";
//    ACC = @"5603124";
//    SESSION = @"";
//    SecretKey = @"evt6bu4n!sDk";
    
    //--------------------------------------------------//
    NSString* EncodedString = [[StringEncoder sharedMethod]URLEncodedString_ch:TXT];
    
    NSString *CS_String = [[Md5Converter sharedMethod]MD5String:[NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",EID,LID,VID,TXT,EXT,FX_TYPE,FX_LEVEL,ACC,SESSION,SecretKey]];
    //--------------------------------------------------//
    
    NSString *userUpdate =[NSString stringWithFormat:@"http://cache.oddcast.com/tts/gen.php?EID=%@&LID=%@&VID=%@&TXT=%@&EXT=%@&FX_TYPE=%@&FX_LEVEL=%@&ACC=%@&SESSION=%@&CS=%@",EID,LID,VID,EncodedString,EXT,FX_TYPE,FX_LEVEL,ACC,SESSION,[CS_String lowercaseString]];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ROFL"
//                                                    message: userUpdate
//                                                   delegate:self
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];

    
//    NSString *userUpdate =[NSString stringWithFormat:@"http://cache.oddcast.com/tts/gen.php?EID=%@&LID=%@&VID=%@&TXT=%@&EXT=%@&FX_TYPE=&FX_LEVEL=&ACC=%@&SESSION=&CS=%@",@"2",@"19",@"2",EncodedString,@"mp3",@"5603124",@"3ff28e266dcaa2db7b20e484e5d8801c"];
    
    NSError *err;
    NSURLResponse *response;
    
//    NSMutableURLRequest *AudioRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://cache.oddcast.com/tts/gen.php?"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
//    [AudioRequest setHTTPMethod:@"GET"];
//    NSString *postString = [NSString stringWithFormat:@"EID=%@&LID=%@&VID=%@&TXT=%@&EXT=%@&FX_TYPE=%@&FX_LEVEL=%@&ACC=%@&SESSION=%@&CS=%@",EID,LID,VID,EncodedString,EXT,FX_TYPE,FX_LEVEL,ACC,SESSION,[CS_String lowercaseString]];
//    [AudioRequest setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
//    NSData *AudioRequestData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
//    NSString* AudioResponseString = [[NSString alloc]initWithData:AudioRequestData encoding:NSASCIIStringEncoding];
//    
//    NSLog(@"AudioResponseString ::::::    %@", AudioResponseString);
    
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data1];
    
    NSString *resSrt ;
    
    if(appDelegate_obj.ReturnConnection)
    {
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        resSrt = [[NSString alloc]initWithData:responseData encoding:NSASCIIStringEncoding];
        
//        Error: [101] Missing required parameter: Missing EID
//        NSLog(@"got response==%@", resSrt);
        
//        if([resSrt rangeOfString:@"Missing required parameter"].location != NSNotFound)
//        {
        
        NSLog(@"USERUPDATE ARE %@",userUpdate);
        
            return userUpdate;
        
       // }
       // else
       // {
        
            //return resSrt;
      //  }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert!" message:@"This operation requires an internet connection. Your connection seems to be unavailable." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return resSrt;
    }
}

@end
