//
//  voiceIDCell.h
//  VokiApp
//
//  Created by brst on 1/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface voiceIDCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *_voiceID_lbl;

@end
