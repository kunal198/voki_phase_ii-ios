//
//  SaveToCoreData.h
//  VokiApp
//
//  Created by brst on 2/1/16.
//  Copyright © 2016 brst. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AppDelegate;

@interface ConnectionToCoreData : NSObject
{
    AppDelegate *appDelegate;
}

+ (ConnectionToCoreData *)sharedMethod;
-(void)SaveIntoCoreData:(NSMutableArray*)DataArray;
-(NSMutableArray*)FetchDataFromLocalDB;
-(NSMutableArray*)FetchDataWithWhereClause:(NSString*)keyString :(NSString*)ObjString;

@end
